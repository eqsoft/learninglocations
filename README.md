TestRepositoryObject
-------------------
### Installation 

```
mkdir -p Customizing/global/plugins/Services/Repository/RepositoryObject
cd Customizing/global/plugins/Services/Repository/RepositoryObject
git clone https://gitlab.com/eqsoft/LearningLocations.git
```

### Branching
This plugin follows the same branching-rules like the ILIAS-projekt itself:
- trunk: Main-Development-Branch
- release_X-X: Stable Release
