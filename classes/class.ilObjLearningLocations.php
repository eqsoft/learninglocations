<?php

/**
 * This file is part of ILIAS, a powerful learning management system
 * published by ILIAS open source e-Learning e.V.
 *
 * ILIAS is licensed with the GPL-3.0,
 * see https://www.gnu.org/licenses/gpl-3.0.en.html
 * You should have received a copy of said license along with the
 * source code, too.
 *
 * If this is not the case or you just want to try ILIAS, you'll find
 * us at:
 * https://www.ilias.de
 * https://github.com/ILIAS-eLearning
 *
 *********************************************************************/

class ilObjLearningLocations extends ilObjectPlugin implements ilLPStatusPluginInterface
{
    public const PLUGIN_PATH = './Customizing/global/plugins/Services/Repository/RepositoryObject/LearningLocations';

    protected bool $online = false;

    private string $targetTitle = '';

    private string $targetLatitude = '';

    private string $targetLongitude = '';

    private int $targetDistance = 0;

    private string $redirectUrl = '';


    public function __construct($a_ref_id = 0)
    {
        parent::__construct($a_ref_id);
    }

    final protected function initType() : void
    {
        $this->setType(ilLearningLocationsPlugin::ID);
    }

    protected function doCreate(bool $clone_mode = false) : void
    {
        global $ilDB;

        $ilDB->manipulate(
            "INSERT INTO rep_robj_xloc_data " .
            "(id, is_online, target_title, target_latitude, target_longitude, target_distance, redirect_url) VALUES (" .
            $ilDB->quote($this->getId(), "integer") . "," .
            $ilDB->quote(0, "integer") . "," .
            $ilDB->quote($this->getTargetTitle(), "text") . "," .
            $ilDB->quote($this->getTargetLatitude(), "text") . "," .
            $ilDB->quote($this->getTargetLongitude(), "text") . "," .
            $ilDB->quote($this->getTargetDistance(), "integer") . "," .
            $ilDB->quote($this->getRedirectUrl(), "text") .
            ")"
        );
    }

    protected function doRead() : void
    {
        global $ilDB;

        $set = $ilDB->query(
            "SELECT * FROM rep_robj_xloc_data " .
            " WHERE id = " . $ilDB->quote($this->getId(), "integer")
        );
        while ($rec = $ilDB->fetchAssoc($set)) {
            $this->setOnline($rec["is_online"]);
            $this->setTargetTitle($rec["target_title"]);
            $this->setTargetLatitude($rec["target_latitude"]);
            $this->setTargetLongitude($rec["target_longitude"]);
            $this->setTargetDistance($rec["target_distance"]);
            $this->setRedirectUrl($rec["redirect_url"]);
        }
    }

    protected function doUpdate() : void
    {
        global $ilDB;

        $ilDB->manipulate(
            $up = "UPDATE rep_robj_xloc_data SET " .
                " is_online = " . $ilDB->quote($this->isOnline(), "integer") . "," .
                " target_title = " . $ilDB->quote($this->getTargetTitle(), "text") . "," .
                " target_latitude = " . $ilDB->quote($this->getTargetLatitude(), "text") . "," .
                " target_longitude = " . $ilDB->quote($this->getTargetLongitude(), "text") . "," .
                " target_distance = " . $ilDB->quote($this->getTargetDistance(), "integer") . "," .
                " redirect_url = " . $ilDB->quote($this->getRedirectUrl(), "text") .
                " WHERE id = " . $ilDB->quote($this->getId(), "integer")
        );
    }

    protected function doDelete() : void
    {
        global $ilDB;

        $ilDB->manipulate(
            "DELETE FROM rep_robj_xloc_data WHERE " .
            " id = " . $ilDB->quote($this->getId(), "integer")
        );
    }

    protected function doCloneObject($new_obj, $a_target_id, $a_copy_id = null) : void
    {
        //$new_obj->setOnline($this->isOnline());
        $new_obj->update();
    }

    public function setOnline(bool $a_val) : void
    {
        $this->online = $a_val;
    }

    public function isOnline() : bool
    {
        return $this->online;
    }

    // object properties

    public function getTargetTitle(): string {
        return $this->targetTitle;
    }

    public function setTargetTitle(string $a_target_title): void {
        $this->targetTitle = $a_target_title;
    }

    public function getTargetLatitude(): string {
        return $this->targetLatitude;
    }

    public function setTargetLatitude(string $a_target_latidude): void {
        $this->targetLatitude = $a_target_latidude;
    }

    public function getTargetLongitude(): string {
        return $this->targetLongitude;
    }

    public function setTargetLongitude(string $a_target_longitude): void {
        $this->targetLongitude = $a_target_longitude;
    }

    public function getTargetDistance(): int {
        return $this->targetDistance;
    }

    public function setTargetDistance(int $a_target_distance): void {
        $this->targetDistance = $a_target_distance;
    }

    public function getRedirectUrl(): string {
        return $this->redirectUrl;
    }

    public function setRedirectUrl(string $a_redirect_url): void {
        $this->redirectUrl = $a_redirect_url;
    }

    // learningprogress
    public function getLPCompleted() : array
    {
        return array();
    }

    public function getLPNotAttempted() : array
    {
        return array();
    }

    public function getLPFailed() : array
    {
        return array(6);
    }

    public function getLPInProgress() : array
    {
        return array();
    }

    public function getLPStatusForUser(int $a_user_id) : int
    {
        global $ilUser;
        if ($ilUser->getId() == $a_user_id) {
            return $_SESSION[ilObjLearningLocationsGUI::LP_SESSION_ID] ?? 0;
        } else {
            return ilLPStatus::LP_STATUS_NOT_ATTEMPTED_NUM;
        }
    }
}