<#1>
<?php
$fields = array(
	'id' => array(
		'type' => 'integer',
		'length' => 4,
		'notnull' => true
	),
	'is_online' => array(
		'type' => 'integer',
		'length' => 1,
		'notnull' => false
	),
	'target_title' => array(
		'type' => 'text',
		'length' => 250,
		'default' => '',
		'notnull' => true
	),
	'target_latitude' => array(
		'type' => 'text',
		'length' => 250,
		'default' => '',
		'notnull' => true
	),
	'target_longitude' => array(
		'type' => 'text',
		'length' => 250,
		'default' => '',
		'notnull' => true
	),
	'target_distance' => array(
		'type' => 'integer',
		'length' => 4,
		'notnull' => true
	),
	'redirect_url' => array(
		'type' => 'text',
		'length' => 250,
		'default' => '',
		'notnull' => false
	)
);
if(!$ilDB->tableExists("rep_robj_xloc_data")) {
	$ilDB->createTable("rep_robj_xloc_data", $fields);
	$ilDB->addPrimaryKey("rep_robj_xloc_data", array("id"));
}
?>